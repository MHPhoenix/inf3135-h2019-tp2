#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

bool calculNombrePremier (unsigned long long int nombre) {

	unsigned long long int racineCarre = sqrt ( nombre );

	if (nombre  == 2) {
		return true;
	}

	if ( (nombre % 2) == 0) {
		return false;
	} else {

		int i;

		for (i = 3; i <= racineCarre; i = i + 2) {

			if ( (nombre % i) == 0) {
				return false;
			}
		}

		return true;
	}
}


void calculNombreParfait (FILE * fichierSortie, const unsigned long long int  debut, const unsigned long long int fin) {


	unsigned long long int nombrePremier=2;
	unsigned long long int nombreParfait;

	while ( (nombrePremier < fin) && (nombrePremier < 32) ) {

		if ( calculNombrePremier(nombrePremier) ) {

			if ( calculNombrePremier(pow(2,nombrePremier) - 1) ) {

				nombreParfait = ( (pow(2, nombrePremier - 1)) * (pow(2,nombrePremier) - 1) );

				if (nombreParfait >= debut && nombreParfait <= fin ) {

					fprintf (fichierSortie,"%llu\n", nombreParfait);

				}
			}

		     }

			nombrePremier++;
		}
}


void lecteurFichier (FILE * fichierALire, FILE * fichierAEcrire) {

        unsigned long long int valeurDebut = 0;
        unsigned long long int valeurFin = 0;
        unsigned long long int temporaire = 0;

        if (fichierALire != NULL) {

		//fscanf pour stocker les valeurs du fichier
                fscanf (fichierALire, "%llu  %llu", &valeurDebut, &valeurFin);

                if (valeurDebut > valeurFin) {

                        temporaire = valeurDebut;
                        valeurDebut = valeurFin;
                        valeurFin = temporaire;
                }

                calculNombreParfait (fichierAEcrire, valeurDebut, valeurFin);

        } else {
                exit(5);
        }
}


void comportementArguments (FILE *fichierDEntre, FILE *fichierDeSorti, int argument_c, int argument_i, int argument_o) {

	char codePermanent [13] = "MONH08519906";

	if (argument_i == 1) {
                fclose (fichierDEntre);
        }

	if (argument_o == 1) {
                fclose (fichierDeSorti);
        }

	if (argument_c == 1) {

                FILE *fichierASortir = fopen ("code.txt", "w");
                fputs (codePermanent, fichierASortir);
                fclose (fichierASortir);
        }

}


void gestionArguments(int argc, char *argv[]) {

	FILE * fichierDEntre = stdin;
        FILE * fichierDeSorti = stdout;
        char nomFichierEntre [200];
        char nomFichierSortie [200];
        char codePermanent [13];
        int argument_c = 0;
        int argument_i = 0;
        int argument_o = 0;
        int i;

        for (i = 1; i < argc; i = i+2) {

                if ( strcmp (argv [i], "-c") == 0) {

                        //code permanent
                        argument_c = 1;

			if (i < ( argc-1)) {
                        	if (strlen (argv [i + 1]) == 12) {
                                strcpy (codePermanent, argv [i + 1]);

                        	} else {
                                	exit (2);
                        	}
			}

			else  {
				fprintf(stderr, "Usage: %s <-c CODEpermanent> [-i fichier.in] [-o fichier.out] \n", argv[0]);
				exit(1);
			}

                } else if ( strcmp (argv [i], "-i") == 0) {

                        //fichier d'entree
                        argument_i = 1;

                        if (strlen (argv [i + 1] ) != 0) {
                                strcpy (nomFichierEntre, argv [i + 1]);
                                fichierDEntre = fopen (nomFichierEntre, "r");
                        }

                } else if ( strcmp (argv [i], "-o") == 0 ) {

                        // fichier de sortie
                        argument_o = 1;

                        if (strlen (argv [i + 1]) != 0) {
                                strcpy (nomFichierSortie, argv [i + 1]);
                                fichierDeSorti = fopen (nomFichierSortie, "w");

				if (fichierDeSorti == NULL) {
                                        exit (6);
                                }
                        }

                } else if ( (strcmp (argv [i], "-c") != 0) || ( strcmp (argv [i], "-i") != 0)
                                || ( strcmp (argv [i], "-o") != 0) ) {
                        exit(3);
                }

        }


	if ( ( (argument_i == 0) && (argument_o == 0) && (argument_c == 0) ) ||
             (  ((argument_i == 1) || (argument_o == 1))  && (argument_c == 0)  )    ) {

        	fprintf(stderr, "Usage: %s <-c CODEpermanent> [-i fichier.in] [-o fichier.out] \n", argv[0]);

       		exit(1);
        }

        lecteurFichier (fichierDEntre, fichierDeSorti);

	comportementArguments (fichierDEntre, fichierDeSorti, argument_c, argument_i, argument_o);
}
