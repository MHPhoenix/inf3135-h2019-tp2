# Travail pratique 2

## Description

Le cours de construction et maintenance de logiciel (INF3135), qui se donne
a l'Universite du Quebec a Montreal, a pour but de nous initier en
construction et maintenance de logiciel et au langage C.

Le deuxieme Travail pratique, est une maintenance du travail pratique 1 ou on automatise les tests avec
fichier bash. 

## Auteur

Houefa Orphyse Peggy Merveille MONGBO (MONH08519906)

## Fonctionnement

Le programme peut être lancé en ligne de commande avec au minimum les deux syntaxes suivantes :

> $ ./tp2 -c CODE_permanent -i nom_du_fichier_en_entree.ext -o fuchier_sortie.ext

> $ ./tp2 -c CODE_permanent < nom_du_fichier_en_entree.ext > fichier_sortie.ext

## Contenu du projet

> **Le projet contient les fichiers suivants :**

> - cp.txt: contient mon code permanent complet en majuscule.
> - tp2.c : contient la fonction main.
> - README.md : contient la description du projet.
> - Makefile : supporte les appels make, make clean, make data, make test et make resultat.
> - .gitignore : contient les fichiers . et ce qu'on ce ne veut pas faire apparaitre dans git status.
> - outils.c : contient le code source du projet (inf3135-h2019-tp2)
> - outil.h : contient les prototypes des fonctions de outils.c
> - evaluser.sh : le bash qui automatise les tests et retourne la note totale

## References

> **Documentation :**

> - notes de cours
> - OpenClassRoom

## Statut

Fonctionnel


