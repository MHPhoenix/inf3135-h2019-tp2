
default: outils.o tp2.o outils.h
	gcc -Wall -pedantic -std=c99  -o tp2 -lm tp2.o outils.o

tp2.o: tp2.c
	gcc -Wall -pedantic -std=c99  -c tp2.c

outils.o: outils.c
	gcc -Wall -pedantic -std=c99  -c outils.c

test:
	for testeur in `ls data/*.*` ; do \
	./tp2 -c $(CP) -i $$testeur >> resultat.txt; \
	done

clean:
	rm -rf  *.o
	rm -rf tp2
	rm -rf code.txt

data:
	wget -N https://www.github.com/guyfrancoeur/INF3135_H2019/raw/master/tp1/data.zip
	unzip -o data -d ./data

.phony: data clean

resultat:
	git add resultat.txt
	git commit -m "MAKE RESULTAT"
	git push origin master

CP = `cat cp.txt`




