bool calculNombrePremier (unsigned long long int nombre);
void lecteurFichier (FILE * fichierALire, FILE * fichierAEcrire);
void comportementArguments (FILE *fichierDEntre, FILE *fichierDeSorti, int argument_c, int argument_i, int argument_o);
void gestionArguments(int argc, char *argv[]);
void calculNombreParfait (FILE * fichierSortie, const unsigned long long int debut, const unsigned long long int fin);


